async function sendEmail(email){
    try {
        const emailRes = await fetch('http://email-service:3001/email/send', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(email)
        })
        if (emailRes.status === 201) {
            return true
        }
    } catch (err) {
        console.log(err)
        throw new Error(`An error has ocurred at new subscription, err: ${err}`)
    }
    return false
}

module.exports = { sendEmail }