const Subscription = require('../../schemas/subscription')
const { sendEmail } = require('../emails/send-email')

async function newSubscription(subscriptionRaw){
    try {
        const subscription = new Subscription(subscriptionRaw)
        await subscription.save()
        const emailSent = await sendEmail({
            subject: `You've signed to the newsleter`,
            body: `Welcome to Adidas, you've succesfully signed to our newsletter!`,
            email: subscription.email
        })
        return subscription
    } catch (err) {
        console.log(err)
        throw new Error(`An error has ocurred at new subscription, err: ${err}`)
    }
}

module.exports = { newSubscription }