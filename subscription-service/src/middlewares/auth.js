const User = require('../schemas/user')

async function authMiddleware(req, res, next) {
    try {
        const user = await User.findOne({token: req['headers'].token})
        if (user) {
            next()
            return
        }
    } catch(err) {
        console.log(err);
    }
    return res.sendStatus(401) 
}

module.exports = authMiddleware