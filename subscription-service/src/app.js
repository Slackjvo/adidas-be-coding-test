require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const subscriptionRouter = require('./routers/subscription.js')
const createAdminUser = require('../scripts/admin-user')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')

if (process.env.NODE_ENV !== 'test') {
    const mongoDB = `mongodb://dbmongo:27017/${process.env.DB}`
    mongoose.connect(mongoDB)
    createAdminUser() //Only for test purposes
}

const app = express()

app.get('/', (req, res) => {
    res.send('Subscription Service running!')
})

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

app.use(express.json())
app.use('/subscriptions', subscriptionRouter)

const appInstance = app.listen(3002, () => console.log('Subscription service running'))

module.exports = appInstance