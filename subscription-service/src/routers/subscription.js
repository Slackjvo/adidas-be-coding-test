const express = require('express')
const router = express.Router()
const Subscription = require('../schemas/subscription')
const { newSubscription } = require('../services/subscriptions/new-subscription')
const authMiddleware = require('../middlewares/auth')

router.post('/', async(req, res) => {
    try {
        const subscription = await newSubscription(req.body)
        res.status(201).send(subscription._id)
    } catch (err) {
        console.log(err);
        res.status(400).send(err)
    }
})

router.get('/', authMiddleware,  async(req, res) => {
    try {
        const subscriptions = await Subscription.find({}).select({ "_id": 1, "email": 1})
        res.send(subscriptions)
    } catch (err) {
        res.status(500).send(err)
    }
})

router.get('/:id', authMiddleware,  async(req, res) => {
    try {
        const subscription = await Subscription.findById(req.params.id)
        if (!subscription) {
            res.sendStatus(404)
        } else {
            res.send(subscription)
        }
    } catch (err) {
        res.status(500).send(err)
    }
})


router.delete('/:id', authMiddleware,  async(req, res) => {
    try {
        const subscription = await Subscription.findByIdAndDelete(req.params.id)
        if (!subscription) {
            res.sendStatus(404)
        } else {
            res.status(204).send(subscription)
        }
    } catch (err) {
        console.log(err)
        res.status(500).send(err)
    }
})

module.exports = router