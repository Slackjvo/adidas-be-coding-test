const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        match: /^[\w-\.]+@[\w-]+\.[\w-]+$/
    },
    first_name: {
        type: String,
        required: false,
    },
    gender: {
        type: String,
        enum: ["Male", "Female", "Unknown"],
        required: false
    },
    date_of_birth: {
        type: String,
        match: /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/,
        required: true,
    },
    flag_consent: {
        type: Boolean,
        required: true,
    },
    campaign_id: {
        type: String,
        required: true
    }
})

schema.index({ email: 1, campaign_id: 1}, { unique: true })

const Subscription = mongoose.model('Subscription', schema, 'subscriptions')

module.exports = Subscription