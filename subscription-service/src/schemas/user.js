const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        match: /^[\w-\.]+@[\w-]+\.[\w-]+$/
    },
    password: {
        type: String,
        required: false,
    },
    token: {
        type: String,
        required: true
    }
})

schema.index({ email: 1 }, { unique: true })

const User = mongoose.model('User', schema, 'users')

module.exports = User