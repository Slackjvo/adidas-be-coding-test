process.env.NODE_ENV = 'test'
const request = require('supertest')
const app = require('../src/app')
const Subscription = require('../src/schemas/subscription')
const User = require('../src/schemas/user')
const emailServiceModule = require('../src/services/emails/send-email')

const userAllowedMock = {
    _id: '601237c1b3123dc7232141',
    email: 'admin@admin.com',
    password: 'admin',
    token: 'token'
}

jest.mock('../src/services/emails/send-email', () => ({
    sendEmail: jest.fn(),
}))

afterAll(() => {
    app.close()
})

describe('Subscriptions Router Endpoints', () => {

   describe('GET /subscriptions', () => {
        test('Should return all subscriptions', async () => {

            const subscriptionsMocks = [
                {
                    _id: '606bb1b7c1bdc721641d7e91',
                    email: 'ejemplo1@ejemplo.com',
                    date_of_birth: '01/01/2000',
                    flag_consent: true,
                    campaign_id: '1234',
                    first_name: 'Juan',
                    gender: 'Male'
                }
            ]
            
            User.findOne = jest.fn().mockResolvedValue(userAllowedMock)

            jest.spyOn(Subscription, 'find').mockImplementation(() => {
                return { 
                    select: jest.fn().mockResolvedValueOnce(subscriptionsMocks)
                }
            })

            const res = await request(app)
                .get('/subscriptions')
                .set('token', 'tokentest')

            expect(res.body).toEqual(subscriptionsMocks)
        })

        test('Should return 401 with token invalid', async () => {
            User.findOne = jest.fn().mockResolvedValue(null)

            const res = await request(app)
            .get('/subscriptions')
            .set('token', 'tokentest')
            .expect(401)
        })
    })

    describe('POST /subscriptions', () => {
        test('Should create new subscription', async () => {

            emailServiceModule.sendEmail.mockImplementation(() => true)

            const subscriptionMock = {
                email: 'test@test.com',
                date_of_birth: '2003-03-03',
                flag_consent: true,
                campaign_id: '1234',
                first_name: 'Juan',
                gender: 'Male'
            }

            Subscription.prototype.save = jest.fn().mockResolvedValue(Promise.resolve(
                {_id: "2123", ...subscriptionMock}
            ))
            
            const res = await request(app)
                .post('/subscriptions')
                .send(subscriptionMock)

            expect(res.statusCode).toBe(201)
        })

        test('Should return 400 because required field are missing', async () => {

            emailServiceModule.sendEmail.mockImplementation(() => true)

            const subscriptionMock = {
                date_of_birth: '2003-03-03',
                flag_consent: true,
                campaign_id: '1234',
                first_name: 'Juan',
                gender: 'Male'
            }

            jest.spyOn(Subscription.prototype, 'save').mockImplementationOnce(function() {
                const subscription = new Subscription(subscriptionMock)
                const validationError = subscription.validateSync()
                if (validationError) {
                    throw new Error(validationError)
                }
                subscription._id = '132'
                return Promise.resolve(subscription)
            })
            
            const res = await request(app)
                .post('/subscriptions')
                .send(subscriptionMock)

            expect(res.statusCode).toBe(400)
        })
    })

    describe('DELETE /subscriptions/:id', () => {
        it('Should delete the subscription', async () => {
            
            User.findOne = jest.fn().mockResolvedValue(userAllowedMock)

            const subscription = new Subscription({
                email: 'test@test.com',
                date_of_birth: '2003-03-03',
                flag_consent: true,
                campaign_id: '1234',
                first_name: 'Juan',
                gender: 'Male'
            })

            jest.spyOn(Subscription.prototype, 'save').mockImplementation(() => {
                return Promise.resolve({ _id: '123', ...subscription })
            })
            
            await subscription.save()

            jest.spyOn(Subscription, 'findByIdAndDelete').mockResolvedValueOnce(subscription)
        
            const res = await request(app)
                .delete(`/subscriptions/${subscription._id}`)
                .set('token', 'tokentest')
        
            expect(res.statusCode).toBe(204)
        })

        it('Should return 401 with token invalid', async () => {

            User.findOne = jest.fn().mockResolvedValue(null)
            
            await request(app)
                .delete(`/subscriptions/123`)
                .set('token', '3333')
                .expect(401)
        })
    })
})
        