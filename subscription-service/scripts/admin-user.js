const User = require('../src/schemas/user')

async function createAdminUser(){
    try {
        const subscription = new User({
            email: "admin@admin.com",
            password: "admin",
            token: "adminToken"
        })
        await subscription.save()
    } catch (err) {
        console.log(`Error at creating admin user: ${err}`)
    }
}

module.exports = createAdminUser