const request = require('supertest')
const app = require('../app')

afterAll(() => {
    app.close()
})

describe('Email Router Endpoints', () => {
    
    describe('POST /send', () => {

        test('Should return 201 status code', async () => {
            const emailMock = {
                email: 'test@test.com',
                subject: 'test',
                body: 'test'
            }
    
            const response = await request(app).post('/email/send').send(emailMock)
            expect(response.status).toBe(201)
        })
    
        test('Should return 400 status code when fields are incorrect or empty', async () => {
            const emailMock = {
                email: 'test@test.com',
                subject: 'test'
            }
    
            const response = await request(app).post('/email/send').send(emailMock)
            expect(response.status).toBe(400)
        })   

    })
})
