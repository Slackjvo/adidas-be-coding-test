const express = require('express')
const router = express.Router()

router.post('/send', async(req, res) => {
    try {
        const emailObject = req.body
        if ('email' in emailObject && 'subject' in emailObject && 'body' in emailObject) {
            res.sendStatus(201)
        } else {
            res.sendStatus(400)
        }
    } catch(err) {
        console.log(err)
        res.sendStatus(500)
    }
})

module.exports = router