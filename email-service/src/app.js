const express = require('express')
const emailRouter = require('./routers/email.js')

const app = express()

app.get('/', (req, res) => {
    res.send('Email Service running!')
})

app.use(express.json())
app.use('/email', emailRouter)

const appInstance = app.listen(3001, () => console.log('Email service running'))

module.exports = appInstance