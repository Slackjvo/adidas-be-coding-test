const express = require('express')
const subscriptionRouter = require( './routers/subscription.js')

const app = express()

app.get('/', (req, res) => {
    res.sendStatus('Public Service running!')
})

app.use(express.json())
app.use('/subscriptions', subscriptionRouter)

const appInstance = app.listen(3000, () => console.log('Public Service is up!'))

module.exports = appInstance