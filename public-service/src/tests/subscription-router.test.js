const request = require('supertest')
const app = require('../app')

afterAll(() => {
    app.close()
})

describe('Subscriptions Router Endpoints', () => {
    
    describe('POST /subscriptions', () => {

        test('Should return 201 status code', async () => {
            global.fetch = jest.fn(() =>
                Promise.resolve({
                    status: 201,
                    ok: true
                })
            )
    
            const subscriptionMock = {
                email: 'test@test.com',
                date_of_birth: '2003-03-03',
                flag_consent: true,
                campaign_id: '1234',
                first_name: 'Juan',
                gender: 'Male'
            }
    
            const response = await request(app).post('/subscriptions/subscribe').send(subscriptionMock)
            expect(response.status).toBe(201)
        })
    
        test('Should return 500 status code when an error ocurred', async () => {
            global.fetch = jest.fn(() =>
                Promise.resolve(new Error('error'))
            )
    
            const subscriptionMock = {
                email: 'test@test.com',
                date_of_birth: '2003-03-03',
                flag_consent: true,
                campaign_id: '1234',
                first_name: 'Juan',
                gender: 'Male'
            }
    
            const response = await request(app).post('/subscriptions/subscribe').send(subscriptionMock)
            expect(response.status).toBe(500)
        })   

    })
})
