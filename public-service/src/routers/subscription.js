const express = require('express')
const router = express.Router()

router.post('/subscribe', async(req, res) => {
    try {
        const subscriptionRes = await fetch('http://subscription-service:3002/subscriptions', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(req.body)
        })
        res.sendStatus(subscriptionRes.status)
    } catch(err) {
        console.log(err)
        res.sendStatus(500)
    }
})

module.exports = router