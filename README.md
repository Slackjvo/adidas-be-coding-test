# Adidas BE Coding Test
## Requirements  
- Docker
- Node v18
- NPM

## Project dependencies
### Jest  
Jest framework for testing

### Express  
Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.

### Mongoose  
Mongoose provides a straight-forward, schema-based solution to model your application data. It includes built-in type casting, validation, query building, business logic hooks and more, out of the box.

### Swagger  
Swagger allows you to describe the structure of your APIs so that machines can read them. The ability of APIs to describe their own structure is the root of all awesomeness in Swagger.

There is only swagger for the Subscription service, the endpoint is the next one:
```
http://localhost:3002/api-docs
```

### Public service  
Public service comunicates with the Subscription Service, it has only one endpoint that is:
```
localhost:80/subscriptions/subscribe
```
The body request to send can be found at the swagger api docs route, it's the same body as the route post of Subscription

### Subscription service  
Except the request to create a subscription, the other ones need a valid token in headers as the following structure:

```
"token": "adminToken"
```

Actually, there is one user that is created with a token when project starts, you can use the next token for tests purposes:
```
adminToken
```

## Email Service, Public Service And Subscription Service Structure
- src (source)
- src/tests (tests)
- Dockerfile

### Run Project  
You must copy the file inside subscription-service called ".env.dist" and rename it into ".env", is up to you to change the DB name, then you must start the containers, with Docker Compose:

```
docker-compose up -d  
```

You can run each independently with:

```
cd subscription-service && npm start
```

### Tests  
Tests can be executed inside of the next services "subscription-service, email-service and public-service" with

```
cd subscription-service && npm test
```
